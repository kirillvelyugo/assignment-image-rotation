#include <stdio.h>

#include "utils.h"
#include "bmp.h"
#include "image.h"
#include "rotate.h"

int main( int argc, char** argv ) {

    if (argc != 3) {
        printf("Invalid number of arguments. Required arguments: <source-image> <transformed-image>");
        return -1;
    }

    FILE * file_input;

    const enum STATUS_OPEN_FILE read_file_code = open_file(
            argv[1], &file_input, "rb");

    if (read_file_code != 0) {
        printf("Error while reading file. Try again!");
        return read_file_code;
    }

    struct image img_original = {0};

    const enum STATUS_READ read_bmp_image_code = read_bmp_img(
            &img_original, file_input);

    if (read_bmp_image_code != 0){
        printf("Error while creating image struct. Try again!");
        free_img(img_original);
        return read_bmp_image_code;
    }

    FILE * file_output;

    const enum STATUS_OPEN_FILE write_file_code = open_file(
            argv[2], &file_output, "wb");

    if (write_file_code != 0) {
        printf("Error while write to file. Try again!");
        free_img(img_original);
        return write_file_code;
    }

    struct image img_rotated = {0};

    enum STATUS_ROTATE rotate_img_code = rotate_image(&img_rotated, img_original);

    if (rotate_img_code == IMG_MISSING){
        printf("The original image does not exist");
        free_img(img_original);
        free_img(img_rotated);
        return rotate_img_code;
    }

    if (rotate_img_code == ROTATED_FAILED){
        printf("Failed to allocate memory for rotated image");
        free_img(img_original);
        free_img(img_rotated);
        return rotate_img_code;
    }

    enum STATUS_SAVE save_code = save_bmp(img_rotated, file_output);

    if (save_code != 0){
        printf("Error while saving file. Try again!");
        free_img(img_original);
        free_img(img_rotated);
        return save_code;
    }

    free_img(img_original);
    free_img(img_rotated);

    fclose(file_input);
    fclose(file_output);

    return 0;
}
