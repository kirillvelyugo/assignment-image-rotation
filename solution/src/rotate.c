//
// Created by Kirill Velyugo on 03.12.2023.
//

#include "utils.h"
#include "image.h"
#include "rotate.h"

enum STATUS_ROTATE rotate_image (struct image *rotated_img, const struct image img){
    if (img.data == NULL) {
        return IMG_MISSING;
    }

    enum STATUS_READ rotated_img_create_status = generate_image(rotated_img, img.height, img.width);

    if (rotated_img_create_status != MEMORY_ALLOCATION_SUCCESS) return ROTATED_FAILED;

    for(uint64_t i = 0; i < img.height; i++){
        for(uint64_t j = 0; j < img.width; j++){
            rotated_img->data[rotated_img->width * j + rotated_img->width - i - 1] = img.data[img.width * i + j];
        }
    }

    return ROTATED_SUCCESS;
}
