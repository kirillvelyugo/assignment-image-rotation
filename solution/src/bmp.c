//
// Created by Kirill Velyugo on 10.11.2023.
//

#include <stdio.h>

#include "utils.h"
#include "bmp.h"
#include "image.h"

#define BMP_TYPE 0x4d42
#define BMP_BIT_COUNT 24
#define BMP_INFO_SIZE 40
#define BMP_PLANES 1

static uint32_t get_padding (const uint32_t width){
    return (4 - (width * sizeof (struct pixel) % 4)) % 4;
}

static enum STATUS_READ read_pixel (FILE * file, struct image img) {
    for (uint32_t i = 0; i < img.height; i++) {
        if (fread(img.data + (i * img.width), sizeof(struct pixel), img.width, file) != img.width ||
            fseek(file, get_padding(img.width), SEEK_CUR) != 0) {
            return READING_FAILED;
        }
    }

    return SUCCESS_READ;
}


static enum STATUS_READ read_bmp (FILE * file, struct bmp_header * header){

    if (!fread(header, sizeof(struct bmp_header), 1, file))
        return READING_FAILED;

    return SUCCESS_READ;
}

enum STATUS_READ read_bmp_img (struct image * img, FILE * file){
    struct bmp_header bmp_header = {0};

    enum STATUS_READ read_bmp_code = read_bmp(
            file, &bmp_header);

    if (read_bmp_code != 0) {
        printf("Error while reading BMP header. Try again!");
        return read_bmp_code;
    }

    enum STATUS_READ img_create_status =  generate_image(img, bmp_header.biWidth, bmp_header.biHeight);

    if(img_create_status == MEMORY_ALLOCATION_FAILED) return img_create_status;

    if (read_pixel(file, *img) == READING_FAILED){
        return READING_FAILED;
    }

    return SUCCESS_READ;
}

static struct bmp_header generate_bmp_header (const struct image img) {
    const uint32_t img_size = sizeof(struct pixel) * img.height * (img.width + get_padding(img.width));
    const uint32_t file_size = sizeof(struct bmp_header) + img_size;

    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = file_size,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_INFO_SIZE,
            .biWidth = img.width,
            .biHeight = img.height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BIT_COUNT,
    };

    return header;
}

enum STATUS_SAVE save_bmp (const struct image img, FILE * file){
    struct bmp_header header = generate_bmp_header(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, file) == 0) return SAVING_FAILED;


    const uint32_t trash_value = 0;
    const uint32_t padding = get_padding(img.width);

    for (uint32_t i = 0; i < img.height; i++){
        if (fwrite(&img.data[i * img.width], sizeof (struct pixel), img.width, file) != img.width ||
            fwrite(&trash_value, 1, padding, file) != padding){
            return SAVING_FAILED;
        }
    }

    return SUCCESS_SAVE;
}

