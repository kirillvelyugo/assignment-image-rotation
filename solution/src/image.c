//
// Created by Kirill Velyugo on 02.12.2023.
//

#include <stdlib.h>

#include "utils.h"
#include "image.h"

void free_img(struct image image){
    if (image.data != NULL) free(image.data);
}

enum STATUS_READ generate_image (struct image *img, const uint32_t width, const uint32_t height){
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof (struct pixel) * width * height);

    if(img->data == NULL) return MEMORY_ALLOCATION_FAILED;

    return MEMORY_ALLOCATION_SUCCESS;
}
