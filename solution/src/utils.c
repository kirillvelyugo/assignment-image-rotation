//
// Created by Kirill Velyugo on 02.12.2023.
//

#include <stdio.h>

#include "utils.h"

enum STATUS_OPEN_FILE open_file (char * path, FILE ** file, char * mode){
    if ((*file = fopen(path, mode)) == NULL){
        return FAILED_OPEN;
    }

    return SUCCESS_OPEN;
}
