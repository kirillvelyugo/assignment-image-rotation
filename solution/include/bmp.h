//
// Created by Kirill Velyugo on 09.11.2023.
//

#include  <stdint.h>

#include "image.h"
#include "utils.h"

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum STATUS_SAVE {
    SUCCESS_SAVE,
    SAVING_FAILED
};

enum STATUS_READ read_bmp_img (struct image * img, FILE * file);
enum STATUS_SAVE save_bmp (const struct image img, FILE * file);

#endif //IMAGE_TRANSFORMER_BMP_H
