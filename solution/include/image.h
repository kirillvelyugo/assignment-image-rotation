//
// Created by Kirill Velyugo on 10.11.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct pixel {
    uint8_t components[3];
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void free_img(struct image image);
enum STATUS_READ generate_image (struct image *img, const uint32_t width, const uint32_t height);

#endif //IMAGE_TRANSFORMER_IMAGE_H
