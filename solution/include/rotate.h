//
// Created by Kirill Velyugo on 03.12.2023.
//

#include <stdint.h>

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

enum STATUS_ROTATE {
    ROTATED_SUCCESS,
    ROTATED_FAILED,
    IMG_MISSING
};

//struct image rotate_image (struct image img);
enum STATUS_ROTATE rotate_image (struct image *rotated_img, const struct image img);

#endif //IMAGE_TRANSFORMER_ROTATE_H
