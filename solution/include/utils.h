//
// Created by Kirill Velyugo on 03.12.2023.
//

#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

enum STATUS_READ {
    SUCCESS_READ,
    READING_FAILED,
    MEMORY_ALLOCATION_SUCCESS,
    MEMORY_ALLOCATION_FAILED
};

enum STATUS_OPEN_FILE {
    SUCCESS_OPEN,
    FAILED_OPEN
};

enum STATUS_OPEN_FILE open_file (char * path, FILE ** file, char * mode);

#endif //IMAGE_TRANSFORMER_UTILS_H
